import { Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Message } from './entities/chat.entity'; // Import your Message entity

@Injectable()
export class ChatService {
  private readonly logger = new Logger(ChatService.name);

  constructor(
    @InjectRepository(Message)
    private readonly messageRepository: Repository<Message>, // Inject the Message repository
  ) {}

  async handleIncomingMessage(messageData: any): Promise<Message> {
    // Parse incoming message data and create a new message entity
    const newMessage = this.messageRepository.create({
      senderId: messageData.senderId,
      recipientId: messageData.recipientId,
      content: messageData.content,
      // Other relevant fields
    });

    try {
      // Insert the new message entity into the database
      const savedMessage = await this.messageRepository.save(newMessage);

      // Log success or additional processing steps
      this.logger.log(`Message saved: ${savedMessage.id}`);

      return savedMessage;
    } catch (error) {
      // Handle error and log it
      this.logger.error(`Error saving message: ${error.message}`);
      throw new Error('Failed to save message');
    }
  }
}
