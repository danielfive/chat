const WebSocket = require('ws');
const redis = require('redis');
const { createServer } = require('http');
const { Server } = require('socket.io');

// Create a WebSocket server
const httpServer = createServer();
const io = new Server(httpServer);

// Create a Redis client
const redisClient = redis.createClient();

// Mapping of user IDs to WebSocket connections
const userWebSocketMap = {};

// WebSocket connection event handler
io.on('connection', (socket) => {
  // Store the WebSocket connection in the map
  const userId = socket.handshake.query.userId; // Get the user ID from query parameter
  userWebSocketMap[userId] = socket;
});

// Redis pub/sub handling
redisClient.subscribe('chat'); // Subscribe to a Redis channel
redisClient.on('message', (channel, message) => {
  const { userId, content } = JSON.parse(message);
  
  // Look up WebSocket connection from the map
  const recipientSocket = userWebSocketMap[userId];
  
  // Send the message to the recipient's WebSocket connection
  if (recipientSocket) {
    recipientSocket.send(content);
  }
});

// Start the WebSocket server
httpServer.listen(3000, () => {
  console.log('WebSocket server listening on port 3000');
});
